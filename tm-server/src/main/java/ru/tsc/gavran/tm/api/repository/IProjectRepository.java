package ru.tsc.gavran.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    void removeByIndex(@NotNull String userId, final @NotNull Integer index);

    @Nullable
    @SneakyThrows
    Project update(@Nullable Project entity);

    @Nullable
    @SneakyThrows
    Project findByIndex(@NotNull String userId, @NotNull int index);

}