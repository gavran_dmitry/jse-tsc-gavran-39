package ru.tsc.gavran.tm.api.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.IService;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.User;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable
    void close(@NotNull Session session);

    @Nullable
    Session open(@Nullable String login, @Nullable String password);

    void validate(@NotNull Session session, @Nullable Role role);

    void validate(@Nullable Session session);

    @Nullable
    User getUser(@NotNull Session session);

    @Nullable Session sign(@Nullable Session session);

}