package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.IService;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.model.User;

public interface IUserService extends IService<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable void removeByLogin(@Nullable String login);

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User setRole(@Nullable String id, @Nullable Role role);

    @NotNull
    boolean isLoginExist(@NotNull String login);

    @NotNull
    boolean isEmailExist(@NotNull String email);

    @NotNull
    User updateUserById(
            @Nullable String id,
            @Nullable String lastName,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String email
    );

    @NotNull
    User updateUserByLogin(
            @Nullable String login,
            @Nullable String lastName,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String email
    );

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

}