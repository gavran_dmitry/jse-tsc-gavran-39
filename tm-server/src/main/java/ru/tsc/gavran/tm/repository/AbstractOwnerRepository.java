package ru.tsc.gavran.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IOwnerRepository;
import ru.tsc.gavran.tm.model.AbstractOwnerEntity;
import ru.tsc.gavran.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    public AbstractOwnerRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public abstract E add(@NotNull final String userId, @NotNull final E entity);

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ? AND user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public E findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE id = ? AND user_id=? limit 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findById(userId, id) != null;
    }

    @Override
    @SneakyThrows
    public @Nullable void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ? AND user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

}