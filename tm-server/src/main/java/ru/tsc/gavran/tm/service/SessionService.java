package ru.tsc.gavran.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IOwnerRepository;
import ru.tsc.gavran.tm.api.repository.ISessionRepository;
import ru.tsc.gavran.tm.api.service.IConnectionService;
import ru.tsc.gavran.tm.api.service.IServiceLocator;
import ru.tsc.gavran.tm.api.service.ISessionService;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.repository.SessionRepository;
import ru.tsc.gavran.tm.util.HashUtil;

import java.sql.Connection;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IServiceLocator serviceLocator
            )
    {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        if (user.getLocked()) return false;
        @Nullable final String hash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (hash == null) return false;
        return hash.equals(user.getPasswordHash());
    }

    @Override
    @Nullable
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(serviceLocator.getPropertyService(), session);
        session.setSignature(signature);
        return session;
    }


    @Override
    public void close(@NotNull Session session) {
        validate(session);
        remove(session);
    }

    @Override
    @SneakyThrows
    public Session open(@Nullable String login, @Nullable String password) {
        boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = getRepository(connection);
            @Nullable final Session resultSession = sign(session);
            repository.add(session);
            connection.commit();
            return resultSession;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull Session session, @Nullable Role role) throws AccessDeniedException{
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = getRepository(connection);
            if (repository.findById(session.getId()) == null) throw new AccessDeniedException();
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public User getUser(@NotNull final Session session) {
        validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getUserService().findById(userId);
    }

}
