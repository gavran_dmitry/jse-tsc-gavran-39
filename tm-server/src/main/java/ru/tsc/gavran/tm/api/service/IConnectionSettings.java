package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IConnectionSettings {

    @NotNull
    String getJdbcUser();

    @NotNull
    String getJdbcPassword();

    @NotNull
    String getJdbcUrl();

}
