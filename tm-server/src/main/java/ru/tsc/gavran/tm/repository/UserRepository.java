package ru.tsc.gavran.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IUserRepository;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository (@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "app_user";
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull String email) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE email = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeUserById(@NotNull final String id) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeUserByLogin(@NotNull final String login) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public User update(@Nullable final User entity) {
        if (entity == null) return null;
        @NotNull final String query = "update " + getTableName() + " " +
                "set id=?, " +
                "email=?, " +
                "login=?, " +
                "role=?," +
                " locked=?, " +
                "first_name=?, " +
                "last_name=?, " +
                "middle_name=? " +
                "where id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getEmail());
        statement.setString(3, entity.getLogin());
        statement.setString(4, entity.getRole().toString());
        statement.setBoolean(5, entity.getLocked());
        statement.setString(6, entity.getFirstName());
        statement.setString(7, entity.getLastName());
        statement.setString(8, entity.getMiddleName());
        statement.setString(9, entity.getId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    protected User fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setEmail(row.getString("email"));
        user.setLogin(row.getString("login"));
        user.setId(row.getString("id"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setLocked(row.getBoolean("locked"));
        user.setFirstName(row.getString("first_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setLastName(row.getString("last_name"));
        user.setPasswordHash(row.getString("password_hash"));
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User add(@Nullable final User entity) {
        if (entity == null) return null;
        @NotNull final String query = "insert into " + getTableName() +
                " (id, " +
                "email, " +
                "login, " +
                "role, " +
                "locked, " +
                "first_name, " +
                "last_name, " +
                "middle_name, " +
                "password_hash) " +
                "values(?,?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getEmail());
        statement.setString(3, entity.getLogin());
        statement.setString(4, entity.getRole().toString());
        statement.setBoolean(5, entity.getLocked());
        statement.setString(6, entity.getFirstName());
        statement.setString(7, entity.getLastName());
        statement.setString(8, entity.getMiddleName());
        statement.setString(9, entity.getPasswordHash());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

}