package ru.tsc.gavran.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable void removeUserById(@NotNull String id);

    @Nullable
    void removeUserByLogin(@NotNull String login);

    @Nullable
    @SneakyThrows
    User update(User entity);

}