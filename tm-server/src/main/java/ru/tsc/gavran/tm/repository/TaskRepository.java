package ru.tsc.gavran.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.ITaskRepository;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "app_task";
    }

    @Override
    @SneakyThrows
    protected Task fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Task project = new Task();
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setStartDate(row.getDate("start_date"));
        project.setFinishDate(row.getDate("finish_date"));
        project.setCreated(row.getDate("created_date"));
        project.setProjectId(row.getString("project_id"));
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@NotNull String userId, @Nullable Task entity) {
        if (entity == null) return null;
        @NotNull final String query = "insert into " + getTableName() +
                " (" +
                "id, " +
                "name, " +
                "description, " +
                "status, " +
                "start_date, " +
                "finish_date, " +
                "created_date, " +
                "user_id, " +
                "project_id) " +
                "values(?,?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setTimestamp(5, prepare(entity.getStartDate()));
        statement.setTimestamp(6, prepare(entity.getFinishDate()));
        statement.setTimestamp(7, prepare(entity.getCreated()));
        statement.setString(8, userId);
        statement.setString(9, entity.getProjectId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task findByName(@NotNull String userId, @NotNull final String name) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE name = ? AND user_id=? limit 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final Task result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull String userId, @NotNull final String name) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE name = ? AND user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public Task startById(@NotNull String userId, @NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task startByName(@NotNull String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task startByIndex(@NotNull final String userId, @NotNull final int index) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task finishByIndex(@NotNull final String userId, @NotNull final int index) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final int index, @NotNull final Status status) {
        @Nullable final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String id) {
        return findAll(userId).stream()
                .filter(t -> t.getUserId().equals(userId))
                .filter(t -> t.getProjectId().equals(id))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Task bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        @NotNull final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    @Override
    public Task unbindTaskById(@NotNull String userId, @NotNull String id) {
        final Task task = findById(userId, id);
        task.setProjectId(null);
        return task;
    }

    @NotNull
    @Override
    public void unbindAllTaskByProjectId(@NotNull String userId, @NotNull String id) {
        findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(id))
                .filter(t -> t.getUserId().equals(userId))
                .forEach(t -> t.setProjectId(null));
    }

    public Timestamp prepare(@Nullable final java.util.Date date) {
        if (date == null) return null;
        java.sql.Timestamp mysqlDate = new java.sql.Timestamp(date.getTime());
        return mysqlDate;
    }

    @Nullable
    @Override
    @SneakyThrows
    public  Task update(@Nullable Task entity) {
        if (entity == null) return null;
        @NotNull final String query = "UPDATE " + getTableName() +
                " set id = ?" +
                ", name = ?" +
                ", description=?" +
                ", status=?" +
                ", start_date=?" +
                ", finish_date=?" +
                ", created_date=?" +
                ", user_id=?" +
                ", project_id=?" +
                " WHERE id=? AND user_id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setTimestamp(5, prepare(entity.getStartDate()));
        statement.setTimestamp(6, prepare(entity.getFinishDate()));
        statement.setTimestamp(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.setString(9, entity.getProjectId());
        statement.setString(10, entity.getId());
        statement.setString(11, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findByIndex(@NotNull final String userId, @NotNull int index) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id=? limit 1 offset ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(2, index - 1);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final Task result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @NotNull final int index) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? offset ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index - 1);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task entity) {
        if (entity == null) return null;
        @NotNull final String query = "insert into " + getTableName() + " (" +
                "id, " +
                "name, " +
                "description, " +
                "status, " +
                "start_date, " +
                "finish_date, " +
                "created_date, " +
                "user_id, " +
                "project_id" +
                ") " +
                "values(?,?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setTimestamp(5, prepare(entity.getStartDate()));
        statement.setTimestamp(6, prepare(entity.getFinishDate()));
        statement.setTimestamp(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.setString(9, entity.getProjectId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

}