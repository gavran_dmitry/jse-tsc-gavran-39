package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltSettings, ISessionSettings, IConnectionSettings {

    @NotNull
    Integer getBackupInterval();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

}