package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<Task> findTaskByProjectId(@NotNull String userId, String projectId);

    @NotNull
    Task bindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@NotNull String userId, @Nullable String projectId);

    void removeProjectByIndex(@NotNull String userId, @NotNull int index);

    void removeProjectByName(@NotNull String userId, @Nullable String name);

}
