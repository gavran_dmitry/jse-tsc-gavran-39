package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;

import java.awt.*;

public interface IServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IDataService getDataService();

}