package ru.tsc.gavran.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.service.IPropertyService;
import ru.tsc.gavran.tm.api.service.ISaltSettings;
import ru.tsc.gavran.tm.api.service.ISessionSettings;
import ru.tsc.gavran.tm.model.Session;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @Nullable
    static String salt(@NotNull final ISaltSettings setting, @NotNull final String value) {
        @NotNull final String secret = setting.getPasswordSecret();
        @NotNull final Integer iteration = setting.getPasswordIteration();
        return salt(secret, iteration, value);
    }

    @Nullable
    static String sign(@NotNull final ISessionSettings setting, @NotNull final Object value) {
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return salt(json, setting.getSignatureIteration(), setting.getSignatureSecret().toString());
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    static String salt(@NotNull final String secret, @NotNull final Integer iteration, @NotNull final String value) {
        if (value == null) return null;
        @Nullable
        String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            java.security.MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            @Nullable final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i)
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}