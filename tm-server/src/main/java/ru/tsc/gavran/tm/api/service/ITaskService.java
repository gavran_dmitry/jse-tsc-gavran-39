package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Task;

public interface ITaskService extends IOwnerService<Task> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    Task changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @Nullable
    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task findByName(@Nullable String userId, @Nullable String name);

    @Nullable void removeByName(@Nullable String userId, @Nullable String name);

    @NotNull void removeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @Nullable
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Task startById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task startByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task startByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Task finishById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task finishByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task findByIndex(@Nullable final String userId, @Nullable final Integer index);

}