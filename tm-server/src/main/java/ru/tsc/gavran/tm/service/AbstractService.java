package ru.tsc.gavran.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.api.IService;
import ru.tsc.gavran.tm.api.service.IConnectionService;
import ru.tsc.gavran.tm.exception.empty.EmptyIdException;
import ru.tsc.gavran.tm.exception.empty.EmptyIndexException;
import ru.tsc.gavran.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public abstract IRepository<E> getRepository(@NotNull Connection connection);

    @NotNull
    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E add(@Nullable final E entity) {
        if (entity == null) return null;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            @Nullable final E entityResult = repository.add(entity);
            connection.commit();
            return entityResult;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.remove(entity);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final List<E> entities) {
        if (entities == null) return;
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.addAll(entities);
            connection.commit();
        }catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}