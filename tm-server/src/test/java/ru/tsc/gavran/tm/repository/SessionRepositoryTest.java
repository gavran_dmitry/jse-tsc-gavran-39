package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.service.ConnectionService;
import ru.tsc.gavran.tm.service.PropertyService;
import ru.tsc.gavran.tm.service.SessionService;

import java.util.List;

public class SessionRepositoryTest {

    @Nullable
    private SessionRepository sessionRepository;

    @Nullable
    private SessionService sessionService;

    @Nullable
    private Session session;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionRepository = new SessionRepository(connectionService.getConnection());
        @NotNull final Session session = new Session();
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Admin11", "Admin11");
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        @NotNull final Session sessionById = sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session.getId(), sessionById.getId());
    }

    @Test
    public void findById() {
        @NotNull final Session session = sessionRepository.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionRepository.findAll();
        Assert.assertNotNull(session);
        Assert.assertTrue(session.size() > 0);
    }

}