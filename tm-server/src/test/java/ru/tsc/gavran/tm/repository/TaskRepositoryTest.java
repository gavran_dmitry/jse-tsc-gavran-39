package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.service.ConnectionService;
import ru.tsc.gavran.tm.service.PropertyService;
import ru.tsc.gavran.tm.service.SessionService;

import java.util.List;

public class TaskRepositoryTest {

    @Nullable
    private TaskRepository taskRepository;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Task task;

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_TASK_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Admin11", "Admin11");
        task = new Task();
        task.setName(TEST_TASK_NAME);
        task.setDescription(TEST_DESCRIPTION_NAME);
        task.setUserId(session.getUserId());
        taskRepository = new TaskRepository(connectionService.getConnection());
        task = taskRepository.add(this.session.getUserId(), task);
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertNotNull(tasks);
    }

    @Test
    public void existsById() {
        Assert.assertTrue(taskRepository.existsById(session.getUserId(), task.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskRepository.findAll(session.getUserId());
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findById() {
        @NotNull final Task task = taskRepository.findById(session.getUserId(), this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    public void remove() {
        taskRepository.removeById(task.getId());
        Assert.assertNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void findByName() {
        @NotNull final Task task = taskRepository.findByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
    }

    @Test
    public void findByIndex() {
        @NotNull final Task task = taskRepository.findByIndex(session.getUserId(), 1);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeById() {
        taskRepository.removeById(session.getUserId(), task.getId());
        Assert.assertNull(taskRepository.findById(task.getId()));
    }

     @Test
    public void removeByName() {
        taskRepository.removeByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNull(taskRepository.findByName(session.getUserId(), TEST_TASK_NAME));
    }

    @Test
    public void startById() {
        @Nullable final Task task = taskRepository.startById(session.getUserId(), this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void startByName() {
        @Nullable final Task task = taskRepository.startByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
   }

    @Test
    public void startByIndex() {
        @Nullable final Task task = taskRepository.startByIndex(session.getUserId(), 1);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void finishById() {
        @Nullable final Task task = taskRepository.finishById(session.getUserId(), this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void finishByName() {
        @Nullable final Task task = taskRepository.finishByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void changeStatusById() {
        @Nullable final Task task = taskRepository.changeStatusById(session.getUserId(), this.task.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    public void changeStatusByName() {
        @Nullable final Task task = taskRepository.changeStatusByName(session.getUserId(), TEST_TASK_NAME, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

}