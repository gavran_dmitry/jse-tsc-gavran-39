package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.service.ConnectionService;
import ru.tsc.gavran.tm.service.PropertyService;
import ru.tsc.gavran.tm.service.SessionService;

import java.util.List;

public class ProjectRepositoryTest {

    @Nullable
    private ProjectRepository projectRepository;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Project project;

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_PROJECT_NAME = "Project1";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "Project1Desc";

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Admin11", "Admin11");
        projectRepository = new ProjectRepository(connectionService.getConnection());
        project = new Project();
        project.setName(TEST_PROJECT_NAME);
        project.setDescription(TEST_DESCRIPTION_NAME);
        project = projectRepository.add(session.getUserId(), project);
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals(TEST_PROJECT_NAME, project.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, project.getDescription());

        @NotNull final Project projectById = projectRepository.findById(project.getId());
        Assert.assertNotNull(projectById);
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertNotNull(project);
    }

    @Test
    public void existsById() {
        Assert.assertTrue(projectRepository.existsById(session.getUserId(), project.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectRepository.findAll(TEST_USER_ID);
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findById() {
        @NotNull final Project project = projectRepository.findById(session.getUserId(), this.project.getId());
        Assert.assertNotNull(project);
    }


    @Test
    public void findByName() {
        @NotNull final Project project = projectRepository.findByName(session.getUserId(), TEST_PROJECT_NAME);
        Assert.assertNotNull(project);
    }

    @Test
    public void findByIndex() {
        @NotNull final Project project = projectRepository.findByIndex(session.getUserId(), 1);
        Assert.assertNotNull(project);
    }

    @Test
    public void removeById() {
        projectRepository.removeById(session.getUserId(), project.getId());
        Assert.assertNull(projectRepository.findById(project.getId()));
    }

}