package ru.tsc.gavran.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.repository.UserRepository;

import java.util.List;

public class UserServiceTest {

    @Nullable
    private UserService userService;

    @Nullable
    private User user;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_USER_LOGIN = "Test11";

    @NotNull
    protected static final String TEST_USER_EMAIL = "Test11@Mail.com";

    @NotNull
    protected static final String TEST_USER_PASSWORD = "Test11Pass";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Admin11", "Admin11");
        userService = new UserService(connectionService, new PropertyService());
        userService.create(TEST_USER_LOGIN,TEST_USER_PASSWORD);
        this.user = userService.findByLogin(TEST_USER_LOGIN);
    }

    @After
    public void after(){
       userService.removeByLogin(TEST_USER_LOGIN);
      sessionService.close(session);
    }

    @Test
    @Ignore
    public void add() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals(TEST_USER_LOGIN, user.getLogin());
        Assert.assertNotNull(user.getEmail());
        Assert.assertEquals(TEST_USER_EMAIL, user.getEmail());
        @Nullable final User userById = userService.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(user.getId(), userById.getId());
    }

    @Test
    public void findAll() {
        @Nullable final List<User> users = userService.findAll();
        Assert.assertTrue(users.size() > 1 );
    }

    @Test
    public void findById() {
        @Nullable final User user = userService.findById(this.user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    public void findByLogin() {
        @Nullable final User user = userService.findByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

    @Test
    public void remove() {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
    }

    @Test
    public void removeById() {
        userService.removeById(user.getId());
        Assert.assertNull(userService.findById(user.getId()));
    }

    @Test
    public void removeUserByLogin() {
        userService.removeByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userService.isLoginExist(this.user.getLogin()));
    }

    @Test
    public void isLoginExistFalse() {
        Assert.assertFalse(userService.isLoginExist("test"));
    }

    @Test
    public void isEmailExistFalse() {
        Assert.assertFalse(userService.isEmailExist("email"));
    }

    @Test
    public void setPassword() {
        @NotNull final User user = userService.setPassword(this.user.getId(), "password");
        Assert.assertNotNull(user);
    }

    @Test
    public void lockUserByLogin() {
        @NotNull final User user = userService.lockUserByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        @NotNull final User user = userService.unlockUserByLogin(this.user.getLogin());
        Assert.assertNotNull(user);
        Assert.assertFalse(user.getLocked());
    }

}