package ru.tsc.gavran.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.gavran.tm.component.Bootstrap;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.repository.ProjectRepository;
import ru.tsc.gavran.tm.repository.TaskRepository;

import java.util.List;

public class ProjectTaskServiceTest {

    @Nullable
    private ProjectTaskService projectTaskService;

    @Nullable
    private ProjectService projectService;

    @Nullable
    private TaskService taskService;

    @Nullable
    private Project project;

    @Nullable
    private Task task;

    @Nullable
    private Session session;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @NotNull
    protected static final String TEST_TASK_NAME = "TestProjectTaskName";

    @NotNull
    protected static final String TEST_PROJECT_NAME = "TestProjectTaskName";

    @NotNull
    protected static final String TEST_PROJECT_TASK_DESCRIPTION = "TestProjectTaskDescription";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        projectService = new ProjectService(new ConnectionService(new PropertyService()));
        taskService = new TaskService(new ConnectionService(new PropertyService()));
        this.session = sessionService.open("Admin11", "Admin11");
        project = new Project();
        project.setName(TEST_PROJECT_NAME);
        project.setDescription(TEST_PROJECT_TASK_DESCRIPTION);
        this.project = projectService.add(session.getUserId(),project);
        task = new Task();
        task.setProjectId(project.getId());
        task.setName(TEST_TASK_NAME);
        task.setDescription(TEST_PROJECT_TASK_DESCRIPTION);
        this.task = taskService.add(session.getUserId(), task);
        projectTaskService = new ProjectTaskService(connectionService);
    }

    @After
    public void after() {
        taskService.remove(session.getUserId(), task);
        projectService.remove(session.getUserId(), project);
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(TEST_TASK_NAME, task.getName());
        Assert.assertEquals(TEST_PROJECT_TASK_DESCRIPTION, task.getDescription());

        @NotNull final Task taskById = taskService.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());

        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals(TEST_PROJECT_NAME, project.getName());
        Assert.assertEquals(TEST_PROJECT_TASK_DESCRIPTION, project.getDescription());
        @NotNull final Project projectById = projectService.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    public void findTaskByProjectId() {
        @NotNull final List<Task> tasks = projectTaskService.findTaskByProjectId(session.getUserId(), project.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getProjectId(), project.getId());
    }

    @Test
    public void bindTaskById() {
        task.setProjectId(null);
        @NotNull final Task bindTask = projectTaskService.bindTaskById(session.getUserId(), project.getId(), task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertNotNull(bindTask.getProjectId());
        Assert.assertEquals(bindTask.getProjectId(), project.getId());
    }

    @Test
    public void unbindTaskById() {
        task.setProjectId(project.getId());
        @NotNull final Task bindTask = projectTaskService.unbindTaskById(session.getUserId(), project.getId(), task.getId());
        Assert.assertNotNull(bindTask);
        Assert.assertNull(bindTask.getProjectId());
    }

 }
