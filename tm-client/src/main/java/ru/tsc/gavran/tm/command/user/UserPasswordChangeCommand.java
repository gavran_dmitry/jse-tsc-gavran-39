package ru.tsc.gavran.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.endpoint.Role;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.endpoint.User;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Optional;

public class UserPasswordChangeCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-change-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change the user's password.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        System.out.println("ENTER USER ID: ");
        @Nullable final String userId = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getAdminUserEndpoint().findUserById(session, userId);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String currentUserId = serviceLocator.getSessionService().getSession().getUserId();
        if (!currentUserId.equals(user.getId())) throw new AccessDeniedException();
        System.out.println("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getAdminUserEndpoint().updateUserPassword(session, password);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}