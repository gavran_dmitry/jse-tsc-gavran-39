package ru.tsc.gavran.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.endpoint.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@Nullable final User user) {
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Name: " + user.getFirstName() + " " + user.getMiddleName() + " " + user.getLastName());
        System.out.println("Email: " + user.getEmail());
        System.out.println("Role: " + user.getRole().value());
        System.out.println("- - - - -");
    }

}