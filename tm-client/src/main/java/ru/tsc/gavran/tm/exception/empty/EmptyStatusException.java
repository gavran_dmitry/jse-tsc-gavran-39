package ru.tsc.gavran.tm.exception.empty;

import ru.tsc.gavran.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty.");
    }

}
