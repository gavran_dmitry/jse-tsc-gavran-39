package ru.tsc.gavran.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gavran.tm.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    AdminDataEndpoint getAdminDataEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    ISessionService getSessionService();

}
