package ru.tsc.gavran.tm.command.data;

import ru.tsc.gavran.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    public AbstractDataCommand() {
    }
}