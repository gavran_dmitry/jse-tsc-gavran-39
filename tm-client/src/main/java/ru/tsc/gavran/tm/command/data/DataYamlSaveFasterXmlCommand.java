package ru.tsc.gavran.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.endpoint.Role;
import ru.tsc.gavran.tm.endpoint.Session;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;

import java.util.Optional;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public Role[] roles() {
        return super.roles();
    }

    @NotNull
    @Override
    public String name() {
        return "data-yaml-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to YAML file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        Optional.ofNullable(session).orElseThrow(AccessDeniedException::new);
        serviceLocator.getAdminDataEndpoint().saveDataYaml(session);
    }

}
