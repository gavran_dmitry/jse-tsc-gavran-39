package ru.tsc.gavran.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.tsc.gavran.tm.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ExistsUserByEmail_QNAME = new QName("http://endpoint.tm.gavran.tsc.ru/", "existsUserByEmail");
    private final static QName _ExistsUserByEmailResponse_QNAME = new QName("http://endpoint.tm.gavran.tsc.ru/", "existsUserByEmailResponse");
    private final static QName _ExistsUserByLogin_QNAME = new QName("http://endpoint.tm.gavran.tsc.ru/", "existsUserByLogin");
    private final static QName _ExistsUserByLoginResponse_QNAME = new QName("http://endpoint.tm.gavran.tsc.ru/", "existsUserByLoginResponse");
    private final static QName _RegistryUser_QNAME = new QName("http://endpoint.tm.gavran.tsc.ru/", "registryUser");
    private final static QName _RegistryUserResponse_QNAME = new QName("http://endpoint.tm.gavran.tsc.ru/", "registryUserResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tsc.gavran.tm.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExistsUserByEmail }
     */
    public ExistsUserByEmail createExistsUserByEmail() {
        return new ExistsUserByEmail();
    }

    /**
     * Create an instance of {@link ExistsUserByEmailResponse }
     */
    public ExistsUserByEmailResponse createExistsUserByEmailResponse() {
        return new ExistsUserByEmailResponse();
    }

    /**
     * Create an instance of {@link ExistsUserByLogin }
     */
    public ExistsUserByLogin createExistsUserByLogin() {
        return new ExistsUserByLogin();
    }

    /**
     * Create an instance of {@link ExistsUserByLoginResponse }
     */
    public ExistsUserByLoginResponse createExistsUserByLoginResponse() {
        return new ExistsUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RegistryUser }
     */
    public RegistryUser createRegistryUser() {
        return new RegistryUser();
    }

    /**
     * Create an instance of {@link RegistryUserResponse }
     */
    public RegistryUserResponse createRegistryUserResponse() {
        return new RegistryUserResponse();
    }

    /**
     * Create an instance of {@link Session }
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByEmail }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gavran.tsc.ru/", name = "existsUserByEmail")
    public JAXBElement<ExistsUserByEmail> createExistsUserByEmail(ExistsUserByEmail value) {
        return new JAXBElement<ExistsUserByEmail>(_ExistsUserByEmail_QNAME, ExistsUserByEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByEmailResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gavran.tsc.ru/", name = "existsUserByEmailResponse")
    public JAXBElement<ExistsUserByEmailResponse> createExistsUserByEmailResponse(ExistsUserByEmailResponse value) {
        return new JAXBElement<ExistsUserByEmailResponse>(_ExistsUserByEmailResponse_QNAME, ExistsUserByEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByLogin }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gavran.tsc.ru/", name = "existsUserByLogin")
    public JAXBElement<ExistsUserByLogin> createExistsUserByLogin(ExistsUserByLogin value) {
        return new JAXBElement<ExistsUserByLogin>(_ExistsUserByLogin_QNAME, ExistsUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsUserByLoginResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gavran.tsc.ru/", name = "existsUserByLoginResponse")
    public JAXBElement<ExistsUserByLoginResponse> createExistsUserByLoginResponse(ExistsUserByLoginResponse value) {
        return new JAXBElement<ExistsUserByLoginResponse>(_ExistsUserByLoginResponse_QNAME, ExistsUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gavran.tsc.ru/", name = "registryUser")
    public JAXBElement<RegistryUser> createRegistryUser(RegistryUser value) {
        return new JAXBElement<RegistryUser>(_RegistryUser_QNAME, RegistryUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistryUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.gavran.tsc.ru/", name = "registryUserResponse")
    public JAXBElement<RegistryUserResponse> createRegistryUserResponse(RegistryUserResponse value) {
        return new JAXBElement<RegistryUserResponse>(_RegistryUserResponse_QNAME, RegistryUserResponse.class, null, value);
    }

}
